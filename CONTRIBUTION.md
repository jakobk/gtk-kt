# Contribution

Contributing to gtk-kt is as easy as reading the following document.

## Standards
- Please use the tab character
- Set your IDE to 80 column guideline, with a 100 column hard limit.

## Pull Request
Pull requests should follow a single idea. 
Examples:
- Implement X
- Fix X

Pull requests should not do multiple things at once.
Pull requests that do multiple things at once are troublesome to check through,
 and commit messages can be difficult to produce.

### How to create a pull request
- Fork the repository
- Add remote branch
  - ``git remote add doomsdayrs https://github.com/Doomsdayrs/gtk-kt.git``
  - ``git fetch doomsdayrs``
- Checkout main branch as it is
  ``git checkout doomsdayrs/gtk-4``
- Fork the branch for your request, examples:
  - ``git checkout -b impl-X``
  - ``git checkout -b fix-Y``
  - ``git checkout -b patch-Z``
- Produce your contribution and commit
- Create a PR