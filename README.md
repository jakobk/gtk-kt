# kotlinx-gtk

GTK bindings for Kotlin via DSL

GTK Version: `4.5.0-alpha`

## Usage

See [example](example/src/nativeMain/kotlin/Main.kt) for a simple example on how to create an
application using this library

## Progress

Check out the [status](src/README.md#Progress) of the wrapping

## Installation

Due to my own limitations, I am unsure of how to properly go around publishing this library onto
maven central. Instead, we will publish to mavenLocal

### Linux

#### Dependencies

Below are the command line instructions to install the needed dependencies to your distribution.
(**Note**: For Ubuntu, the GTK4 dev tools are available only from 21.04 and above.)

| Distro | Command line                            |
| ------ | ------------------------------------------------------------	|
| Fedora | ```sudo dnf install gtk4-devel ncurses-compat-libs```    |
| Ubuntu | ```sudo apt install libgtk-4-dev libncurses5  gcc-multilib```|

#### Local install

Within the working directory

- `gtk`Core library
   ```bash
   ./gradlew -p src/gtk/ publishToMavenLocal
   ```
- `coroutines` Extensions for coroutines
   ```bash
   ./gradlew -p coroutines/ publishToMavenLocal
   ```
- `dsl` DSL bindings to produce UIs easily
   ```bash
   ./gradlew -p dsl/ publishToMavenLocal
   ```
- `ktx` Extension library for convience functions
  ```bash
  ./gradlew -p ktx/ publishToMavenLocal
  ```
- `*` Install all possible files
  ```bash
  ./gradlew publishToMavenLocal
  ```

### Windows

Figure it out

### MacOS

Heh

### Configure your project

After you published to your local maven repository, we will have to configure your projects

1. Add `mavenLocal()` to your own projects repositories
2. Add the dependencies, replacing {VERSION} with the current version
   ```kotlin 
   // Core library
   implementation("org.gnome.gtk:gtk:{VERSION}")
	   
   // DSL library
   implementation("org.gnome.gtk:dsl:{VERSION}")

   // KTX library
   implementation("org.gnome.gtk:ktx:{VERSION}")
   
   // Coroutines
   implementation("org.gnome.gtk:coroutines:{VERSION}")
   implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:{KOTLIN}-native-mt") // Replace {KOTLIN} with current version
   ```

## Contribution

Read the [contribution document](CONTRIBUTION.md).

## Community
[Matrix](https://matrix.to/#/#gtk-kt:matrix.org)

[Slack: kotlinlang/kotlin-native](https://kotlinlang.slack.com/archives/C3SGXARS6)

## Future plans

- Separate DSL out of this project, allowing it to update separately from the project
- Finish more complex parts of this library
