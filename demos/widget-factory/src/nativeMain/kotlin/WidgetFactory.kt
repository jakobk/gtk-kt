import glib.G_SOURCE_REMOVE
import gobject.g_object_get_data
import kotlinx.cinterop.reinterpret
import org.gtk.dsl.gio.*
import org.gtk.dsl.gtk.application
import org.gtk.dsl.gtk.applicationWindow
import org.gtk.dsl.gtk.button
import org.gtk.dsl.gtk.frame
import org.gtk.gdk.Cursor
import org.gtk.gio.ActionEntry
import org.gtk.gio.Application
import org.gtk.gio.SimpleAction
import org.gtk.glib.Variant
import org.gtk.glib.bool
import org.gtk.glib.timeoutAdd
import org.gtk.glib.use
import org.gtk.gtk.Settings
import org.gtk.gtk.widgets.FlowBox
import org.gtk.gtk.widgets.SearchBar
import org.gtk.gtk.widgets.Stack
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.range.Range
import org.gtk.gtk.widgets.windows.Window

/*
 * <a href="https://gitlab.gnome.org/GNOME/gtk/-/blob/master/demos/widget-factory/widget-factory.c">
widget-factory.c</a>
 */


fun changeDarkState(action: SimpleAction, state: Variant) {
	val settings = Settings.default
	settings?.set("gtk-application-prefer-dark-theme", state.boolean)
	action.setState(state)
}

fun changeThemeState(action: SimpleAction, state: Variant) {
	val settings = Settings.default
	val theme: String
	action.setState(state)

	when (state.string) {
		"default" -> {
			theme = "Default"
		}
		"dark" -> {
			theme = "Default-dark"
		}
		"hc" -> {
			theme = "Default-hac"
		}
		"hc-dark" -> {
			theme = "Default-hc-dark"
		}
		"current" -> {
			settings?.resetProperty("gtk-theme-name")
			settings?.resetProperty("gtk-application-prefer-dark-theme")
			return
		}
		else -> return
	}

	settings?.set("gtk-theme-name", theme)
	settings?.set("gtk-application-prefer-dark-theme", false)
}

lateinit var pageStack: Stack

fun transitionSpeedChanged(range: Range) {
	pageStack.transitionDuration = range.value.toUInt()
}

fun changeTransitionState(action: SimpleAction, state: Variant) {
	val transition: Stack.TransitionType =
		if (state.boolean) Stack.TransitionType.CROSSFADE else Stack.TransitionType.NONE

	pageStack.transitionType = transition

	action.setState(state)
}

fun getIdle(window: Window): Boolean {
	val app = window.application

	window.sensitive = true
	window.surface.setCursor(null)
	app?.unmarkBusy()

	return G_SOURCE_REMOVE.bool
}

fun getBusy(window: Window) {
	val app = window.application

	app?.markBusy()

	Cursor("wait", null).use { cursor -> window.native?.surface?.setCursor(cursor) }
	timeoutAdd(5000u) { getIdle(window) }

	window.sensitive = false
}

var currentPage = 0

fun onPage(i: Int): Boolean = currentPage == i

fun activateSearch(window: Window) {
	if (!onPage(2)) return

	val searchbar = SearchBar(g_object_get_data(window.pointer, "searchbar")!!.reinterpret())
	searchbar.searchMode = true
}

fun activateDelete(window: Window) {
	println("Activate action delete")

	if (!onPage(2)) return

	val infobar = Widget(g_object_get_data(window.pointer, "infobar")!!.reinterpret())
	infobar.show()
}

fun activateBackground(window: Window) {
	if (!onPage(2)) return
	//TODO
}


fun populateFlowbox(flowBox: FlowBox) {
	//TODO
}

val appEntries: List<ActionEntry> = listOf(
	actionEntry("about"),
	actionEntry("shortcuts"),
	actionEntry("quit"),
	actionEntry("inspector"),
	stringActionEntry("main", state = "steak"),
	booleanActionEntry("wine", state = false),
	booleanActionEntry("beer", state = false),
	booleanActionEntry("water", state = true),
	stringActionEntry("dessert", state = "bars"),
	stringActionEntry("pay"),
	actionEntry("share"),
	actionEntry("labels"),
	actionEntry("new"),
	actionEntry("open"),
	actionEntry("open-in"),
	actionEntry("open-tab"),
	actionEntry("open-window"),
	actionEntry("save"),
	actionEntry("save-as"),
	actionEntry("cut"),
	actionEntry("copy"),
	actionEntry("paste"),
	booleanActionEntry("pin", state = true),
	stringActionEntry("size", state = "medium"),
	booleanActionEntry("berk", state = true),
	booleanActionEntry("broni", state = true),
	booleanActionEntry("drutt", state = true),
	booleanActionEntry("upstairs", state = true),
	actionEntry("option-a"),
	actionEntry("option-b"),
	actionEntry("option-c"),
	actionEntry("option-d"),
	booleanActionEntry("check-on", state = true),
	booleanActionEntry("check-off", state = false),
	stringActionEntry("radio-x", state = "x"),
	booleanActionEntry("check-on-disabled", state = true),
	booleanActionEntry("check-off-disabled", state = false),
	stringActionEntry("radio-x-disabled", state = "x"),
)

fun main() {
	application("org.gtk.WidgetFactory4", Application.Flags.NONE) {
		//addActionEntries(appEntries, this)
		lookUpSimpleAction("wine") {
			disable()
		}

		lookUpSimpleAction("wine") {
			enable()
		}
		lookUpSimpleAction("check-on-disabled") {
			disable()
		}
		lookUpSimpleAction("check-off-disabled") {
			disable()
		}
		lookUpSimpleAction("radio-x--disabled") {
			disable()
		}
		addMainOption("version", 'v', "Show program version")

		onCreateUI {
			applicationWindow {
				frame {
					button("Test")
				}
			}
		}
		//TODO ENV AUTO QUIT
	}
}