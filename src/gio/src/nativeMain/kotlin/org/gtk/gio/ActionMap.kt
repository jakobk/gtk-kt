package org.gtk.gio

import gio.*
import glib.GVariant
import kotlinx.cinterop.*
import org.gtk.gio.Action.Companion.wrap
import org.gtk.glib.Variant

/**
 * kotlinx-gtk
 *
 * 23 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/iface.ActionMap.html">GActionMap</a>
 */
interface ActionMap {
	val actionMapPointer: CPointer<GActionMap>

	fun addAction(action: Action) {
		g_action_map_add_action(actionMapPointer, action.actionPointer)
	}

	fun addActionEntries(
		entries: List<ActionEntry>,
		userData: Any? = null
	) {
		g_action_map_add_action_entries(
			actionMapPointer,
			memScoped<CValuesRef<GActionEntry>?> {
				allocArrayOf(entries.map { it.actionEntryPointer }).pointed.value
			},
			entries.size,
			@Suppress("RemoveExplicitTypeArguments")
			StableRef.create<NativeActionMapEntryFunction> { actionPointer: CPointer<GSimpleAction>?,
			                                                 parameterPointer: CPointer<GVariant>?,
			                                                 type: Int ->
				entries.find { it.actionEntryPointer == actionPointer }
					?.let { entry ->
						when (type) {
							0 -> entry.activate?.invoke(
								SimpleAction(actionPointer!!),
								Variant(parameterPointer!!),
								userData
							)
							else -> entry.changeState?.invoke(
								SimpleAction(actionPointer!!),
								Variant(parameterPointer!!),
								userData
							)
						}
					}
				Unit
			}.asCPointer()

		)
	}

	fun lookupAction(actionName: String): Action? =
		g_action_map_lookup_action(actionMapPointer, actionName).wrap()

	fun removeAction(actionName: String) {
		g_action_map_remove_action(actionMapPointer, actionName)
	}
}

typealias NativeActionMapEntryFunction = (
	@ParameterName("actionPointer") CPointer<GSimpleAction>?,
	@ParameterName("parameterPointer") CPointer<GVariant>?,
	/**
	 * 0 if its an activate,
	 * 1 if it is a change-state
	 */
	@ParameterName("type") Int,
) -> Unit


typealias ActionMapEntryFunction = ((SimpleAction, Variant, Any?) -> Unit)?