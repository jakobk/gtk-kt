package org.gtk.gio


typealias AsyncReadyCallback = (result: AsyncResult) -> Unit