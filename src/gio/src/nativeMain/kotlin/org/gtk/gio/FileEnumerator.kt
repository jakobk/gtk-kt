package org.gtk.gio

import gio.GFileEnumerator
import gio.g_file_enumerator_get_child
import gio.g_file_enumerator_next_file
import kotlinx.cinterop.CPointer
import org.gtk.gio.File.Companion.wrap
import org.gtk.gio.FileInfo.Companion.wrap

class FileEnumerator(val fileEnumeratorPointer: CPointer<GFileEnumerator>) {

	fun nextFile(): FileInfo? =
		g_file_enumerator_next_file(fileEnumeratorPointer, null, null).wrap()

	fun getFile(fileInfo: FileInfo): File? =
		g_file_enumerator_get_child(fileEnumeratorPointer, fileInfo.fileInfoPointer).wrap()

	companion object {
		inline fun CPointer<GFileEnumerator>?.wrap() =
			this?.wrap()

		inline fun CPointer<GFileEnumerator>.wrap() =
			FileEnumerator(this)
	}
}