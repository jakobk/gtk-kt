package org.gtk.gio

import gio.*
import kotlinx.cinterop.*
import org.gtk.glib.*
import org.gtk.glib.KGBytes.Companion.wrap
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 30 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.InputStream.html">
 *     GInputStream</a>
 */
class InputStream(val inputStreamPointer: GInputStream_autoptr) :
	KGObject(inputStreamPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.clear_pending.html">
	 *     g_input_stream_clear_pending</a>
	 */
	fun clearPending() {
		g_input_stream_clear_pending(inputStreamPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.close.html">
	 *     g_input_stream_close</a>
	 */
	@Throws(KGError::class)
	fun close(cancellable: Cancellable) {
		memScoped {
			val err = allocateGErrorPtr()

			if (
				!g_input_stream_close(
					inputStreamPointer,
					cancellable.cancellablePointer,
					err
				).bool
			) {
				err.unwrap()
			}
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.close_async.html">
	 *     g_input_stream_close_async</a>
	 */
	fun closeAsync(
		ioPriority: Int,
		cancellable: Cancellable,
		callback: AsyncReadyCallback
	) {
		g_input_stream_close_async(
			inputStreamPointer,
			ioPriority,
			cancellable.cancellablePointer,
			staticAsyncReadyCallback,
			StableRef.create(callback).asCPointer()
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.close_finish.html">
	 *     g_input_stream_close_finish</a>
	 */
	@Throws(KGError::class)
	fun closeFinish(result: AsyncResult) {
		memScoped {
			val err = allocateGErrorPtr()

			if (
				!g_input_stream_close_finish(
					inputStreamPointer,
					result.asyncResultPointer,
					err
				).bool
			) {
				err.unwrap()
			}
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.has_pending.html">
	 *     g_input_stream_has_pending</a>
	 */
	val hasPending: Boolean
		get() = g_input_stream_has_pending(inputStreamPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.is_closed.html">
	 *     g_input_stream_is_closed</a>
	 */
	val isClosed: Boolean
		get() = g_input_stream_is_closed(inputStreamPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.read.html">
	 *     g_input_stream_read</a>
	 */
	@Throws(KGError::class)
	fun read(count: ULong, cancellable: Cancellable? = null): Array<Byte> {
		memScoped {
			val buffer = allocArray<ByteVar>(count.toLong())
			val err = allocateGErrorPtr()

			val result = g_input_stream_read(
				inputStreamPointer,
				buffer,
				count,
				cancellable?.cancellablePointer,
				err
			)

			when (result) {
				-1L -> throw err.unwrap(false)!!
				0L -> throw IndexOutOfBoundsException("EOF")
				else -> return Array(count.toInt()) { buffer[it] }
			}
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.read_all.html">
	 *     g_input_stream_read_all</a>
	 * @throws Exception if [onException] is not stated.
	 * @param onException if set, will provide exception that way, and returns
	 * an array of bytes that were collected before the exception occured
	 */
	@Throws(KGError::class)
	fun readAll(
		count: ULong,
		cancellable: Cancellable? = null,
		onException: ((Exception) -> Unit)? = null
	): Array<Byte> {
		memScoped {
			val buffer = allocArray<ByteVar>(count.toLong())
			val err = allocateGErrorPtr()
			val bytesRead = cValue<ULongVar>()

			val r = g_input_stream_read_all(
				inputStreamPointer,
				buffer,
				count,
				bytesRead,
				cancellable?.cancellablePointer,
				err
			).bool

			err.unwrap(false)?.let {
				onException?.invoke(it) ?: throw it
			}

			return if (r) {
				Array(count.toInt()) { buffer[it] }
			} else {
				Array(bytesRead.ptr.pointed.value.toInt()) { buffer[it] }
			}
		}
	}

	/*
	TODO Figure out how to properly use these async
	fun readAllAsync(
		count: ULong,
		ioPriority: Int,
		cancellable: Cancellable? = null,
		callback: AsyncReadyCallback
	) {
		memScoped {
			val buffer = allocArray<ByteVar>(count.toLong())
			g_input_stream_read_all_async(
				inputStreamPointer,
				buffer,
				count,
				ioPriority,
				cancellable?.cancellablePointer,
				staticAsyncReadyCallback,
				StableRef.create(callback).asCPointer()
			)
		}
	}

	fun readAllFinish()

	fun readAsync()
	fun readFinish()
	*/

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.read_bytes.html">
	 *     g_input_stream_read_bytes</a>
	 */
	@Throws(KGError::class)
	fun readBytes(
		count: ULong,
		cancellable: Cancellable? = null
	): KGBytes = memScoped {
		val err = allocateGErrorPtr()

		g_input_stream_read_bytes(
			inputStreamPointer,
			count,
			cancellable?.cancellablePointer,
			err
		)?.wrap() ?: throw err.unwrap(false)!!
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.read_bytes_async.html">
	 *     g_input_stream_read_bytes_async</a>
	 */
	fun readBytesAsync(
		count: ULong,
		ioPriority: Int,
		cancellable: Cancellable? = null,
		callback: AsyncReadyCallback
	) {
		g_input_stream_read_bytes_async(
			inputStreamPointer,
			count,
			ioPriority,
			cancellable?.cancellablePointer,
			staticAsyncReadyCallback,
			StableRef.create(callback).asCPointer()
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.read_bytes_finish.html">
	 *     g_input_stream_read_bytes_finish</a>
	 */
	@Throws(KGError::class)
	fun readBytesFinish(result: AsyncResult) = memScoped {
		val err = allocateGErrorPtr()
		g_input_stream_read_bytes_finish(
			inputStreamPointer,
			result.asyncResultPointer,
			err
		)?.wrap() ?: throw err.unwrap(false)!!
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.set_pending.html">
	 *     g_input_stream_set_pending</a>
	 */
	@Throws(KGError::class)
	fun setPending() {
		memScoped {
			val err = allocateGErrorPtr()
			if (!g_input_stream_set_pending(inputStreamPointer, err).bool)
				err.unwrap()
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.skip.html">
	 *     g_input_stream_skip</a>
	 */
	fun skip(count: ULong, cancellable: Cancellable? = null) = memScoped {
		val err = allocateGErrorPtr()

		g_input_stream_skip(
			inputStreamPointer,
			count,
			cancellable?.cancellablePointer,
			err
		).takeIf { it != -1L } ?: throw err.unwrap(false)!!
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.skip_async.html">
	 *     g_input_stream_skip_async</a>
	 */
	fun skipAsync(
		count: ULong,
		ioPriority: Int,
		cancellable: Cancellable? = null,
		callback: AsyncReadyCallback
	) {
		g_input_stream_skip_async(
			inputStreamPointer,
			count,
			ioPriority,
			cancellable?.cancellablePointer,
			staticAsyncReadyCallback,
			StableRef.create(callback).asCPointer()
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.InputStream.skip_finish.html">
	 *     g_input_stream_skip_finish</a>
	 */
	fun skipFinish(result: AsyncResult) {
		memScoped {
			val err = allocateGErrorPtr()
			g_input_stream_skip_finish(
				inputStreamPointer,
				result.asyncResultPointer,
				err
			).takeIf { it != -1L } ?: throw err.unwrap(false)!!
		}
	}

	companion object{

		inline fun GInputStream_autoptr?.wrap() =
			this?.wrap()

		inline fun GInputStream_autoptr.wrap() =
			InputStream(this)

	}
}