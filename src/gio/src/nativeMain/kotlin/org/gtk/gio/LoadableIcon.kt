package org.gtk.gio

import gio.GLoadableIcon_autoptr
import gio.g_loadable_icon_load
import gio.g_loadable_icon_load_async
import gio.g_loadable_icon_load_finish
import kotlinx.cinterop.StableRef
import kotlinx.cinterop.memScoped
import org.gtk.gio.InputStream.Companion.wrap
import org.gtk.glib.allocateGErrorPtr
import org.gtk.glib.unwrap

/**
 * gtk-kt
 *
 * 26 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/iface.LoadableIcon.html">
 *     GLoadableIcon</a>
 */
interface LoadableIcon : Icon {
	val loadableIconPointer: GLoadableIcon_autoptr

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.LoadableIcon.load.html">
	 *     g_loadable_icon_load</a>
	 */
	fun load(size: Int, cancellable: Cancellable? = null): InputStream {
		memScoped {
			val err = allocateGErrorPtr()

			val inputStream = g_loadable_icon_load(
				loadableIconPointer,
				size,
				null, // TODO Figure out icon type
				cancellable?.cancellablePointer,
				err
			)

			err.unwrap()

			return inputStream!!.wrap()
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.LoadableIcon.load_async.html">
	 *     g_loadable_icon_load_async</a>
	 */
	fun loadAsync(size: Int, cancellable: Cancellable?, callback: AsyncReadyCallback) {
		g_loadable_icon_load_async(
			loadableIconPointer,
			size,
			cancellable?.cancellablePointer,
			staticAsyncReadyCallback,
			StableRef.create(callback).asCPointer()
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.LoadableIcon.load_finish.html">
	 *     g_loadable_icon_load_finish</a>
	 */
	fun loadFinish(result: AsyncResult): InputStream {
		memScoped {
			val err = allocateGErrorPtr()

			val inputStream = g_loadable_icon_load_finish(
				loadableIconPointer,
				result.asyncResultPointer,
				null, // TODO Figure out icon type
				err
			)

			err.unwrap()

			return inputStream!!.wrap()
		}
	}
}