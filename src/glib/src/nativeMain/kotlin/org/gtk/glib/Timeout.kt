package org.gtk.glib

import glib.GSourceFunc
import glib.g_timeout_add
import kotlinx.cinterop.StableRef
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.staticCFunction

/*
 * gtk-kt
 *
 * 10 / 09 / 2021
 *
 * @see <a href=""></a>
 */

private val staticTimeoutFunction: GSourceFunc = staticCFunction { data ->
	val stableRef = data?.asStableRef<SourceFunction>()

	val result = stableRef?.get()?.invoke()

	if (result == false)
		stableRef.dispose()

	result.gtk
}

typealias SourceFunction = () -> Boolean

/**
 * @see <a href="https://docs.gtk.org/glib/func.timeout_add.html">g_timeout_add</a>
 */
fun timeoutAdd(interval: UInt, function: SourceFunction): UInt =
	g_timeout_add(interval, staticTimeoutFunction, StableRef.create(function).asCPointer())