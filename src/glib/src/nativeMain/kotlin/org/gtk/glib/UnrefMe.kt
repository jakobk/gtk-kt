package org.gtk.glib

/**
 * gtk-kt
 *
 * 15 / 10 / 2021
 *
 * Many classes in GTK4 must be unrefed,
 * this interfaces unifies them for unref extension
 */
interface UnrefMe {
	fun unref()
}

inline fun <T : UnrefMe, R> T.use(use: (T) -> R): R =
	use(this).also { unref() }