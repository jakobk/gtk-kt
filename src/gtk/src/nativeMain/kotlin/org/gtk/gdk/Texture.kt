package org.gtk.gdk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.reinterpret
import org.gtk.gio.File
import org.gtk.glib.KGError
import org.gtk.glib.allocateGErrorPtr
import org.gtk.glib.bool
import org.gtk.glib.unwrap
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 10 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/gdk4/class.Texture.html">GdkTexture</a>
 */
class Texture(val texturePointer: CPointer<GdkTexture>) : KGObject(texturePointer.reinterpret()), Paintable {
	override val paintablePointer: CPointer<GdkPaintable> by lazy { texturePointer.reinterpret() }

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/ctor.Texture.new_for_pixbuf.html">gdk_texture_new_for_pixbuf</a>
	 */
	constructor(pixbuf: Pixbuf) : this(gdk_texture_new_for_pixbuf(pixbuf.pixbufPointer)!!)

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/ctor.Texture.new_from_file.html">gdk_texture_new_from_file</a>
	 */
	@Throws(KGError::class)
	constructor(file: File) : this(memScoped {
		val error = allocateGErrorPtr()
		val ptr = gdk_texture_new_from_file(file.filePointer, error)!!
		error.unwrap()
		ptr
	})

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/ctor.Texture.new_from_resource.html">gdk_texture_new_from_resource</a>
	 */
	constructor(resourcePath: String) : this(gdk_texture_new_from_resource(resourcePath)!!)

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/method.Texture.get_height.html">gdk_texture_get_height</a>
	 */
	val height: Int
		get() = gdk_texture_get_height(texturePointer)

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/method.Texture.get_width.html">gdk_texture_get_width</a>
	 */
	val width: Int
		get() = gdk_texture_get_width(texturePointer)

	/**
	 * @see <a href="https://docs.gtk.org/gdk4/method.Texture.save_to_png.html">gdk_texture_save_to_png</a>
	 */
	fun saveToPng(filename: String): Boolean = gdk_texture_save_to_png(texturePointer, filename).bool

	companion object {
		inline fun CPointer<GdkTexture>?.wrap() =
			this?.wrap()

		inline fun CPointer<GdkTexture>.wrap() =
			Texture(this)
	}
}