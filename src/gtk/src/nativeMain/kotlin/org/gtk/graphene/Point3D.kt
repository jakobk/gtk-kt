package org.gtk.graphene

import gtk.graphene_point3d_t
import kotlinx.cinterop.CPointer

/**
 * kotlinx-gtk
 *
 * 27 / 10 / 2021
 *
 * @see <a href=""></a>
 */
class Point3D(val point3D: CPointer<graphene_point3d_t>) {
}