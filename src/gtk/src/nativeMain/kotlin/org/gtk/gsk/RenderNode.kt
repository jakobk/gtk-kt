package org.gtk.gsk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.StableRef
import kotlinx.cinterop.cValue
import kotlinx.cinterop.memScoped
import org.gtk.cairo.Cairo
import org.gtk.glib.*
import org.gtk.glib.KGBytes.Companion.wrap
import org.gtk.graphene.Rect
import org.gtk.graphene.Rect.Companion.wrap
import org.gtk.gsk.RenderNodeType.Companion.valueOf

/**
 * kotlinx-gtk
 *
 * 13 / 10 / 2021
 *
 * @see <a href="https://docs.gtk.org/gsk4/class.RenderNode.html">
 *     GskRenderNode</a>
 */
class RenderNode(val renderNodePointer: CPointer<GskRenderNode>) : UnrefMe {

	fun draw(cr: Cairo) {
		gsk_render_node_draw(renderNodePointer, cr.pointer)
	}

	val bounds: Rect
		get() = memScoped {
			val bounds = cValue<graphene_rect_t>()
			gsk_render_node_get_bounds(renderNodePointer, bounds)
			bounds.ptr.wrap()
		}

	val type: RenderNodeType
		get() = valueOf(gsk_render_node_get_node_type(renderNodePointer))

	fun ref() {
		gsk_render_node_ref(renderNodePointer)
	}

	fun serialize(): KGBytes =
		gsk_render_node_serialize(renderNodePointer)!!.wrap()

	override fun unref() {
		gsk_render_node_unref(renderNodePointer)
	}

	fun writeToFile(filename: String) {
		memScoped {
			val err = allocateGErrorPtr()
			val b = gsk_render_node_write_to_file(renderNodePointer, filename, err)
			if (!b.bool) err.unwrap()
		}
	}


	companion object {

		fun deserialize(bytes: KGBytes, errorFunction: ParseErrorFunction) =
			gsk_render_node_deserialize(
				bytes.pointer,
				staticParseErrorFunction,
				StableRef.create(errorFunction).asCPointer()
			)?.wrap()

		inline fun CPointer<GskRenderNode>?.wrap() =
			this?.wrap()

		inline fun CPointer<GskRenderNode>.wrap() =
			RenderNode(this)
	}

}