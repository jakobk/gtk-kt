package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.CPointer
import org.gtk.glib.UnrefMe
import org.gtk.glib.bool

/**
 * gtk-kt
 *
 * 2 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/struct.Bitset.html">GtkBitset</a>
 */
class KGBitSet(val bitSetPointer: CPointer<GtkBitset>) : UnrefMe {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Bitset.new_empty.html">
	 *     gtk_bitset_new_empty</a>
	 */
	constructor() : this(gtk_bitset_new_empty()!!)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Bitset.new_range.html">
	 *     gtk_bitset_new_range</a>
	 */
	constructor(start: UInt, nItems: UInt) :
			this(gtk_bitset_new_range(start, nItems)!!)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.add.html">
	 *     gtk_bitset_add</a>
	 */
	fun add(value: UInt) =
		gtk_bitset_add(bitSetPointer, value).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.add_range.html">
	 *     gtk_bitset_add_range</a>
	 */
	fun addRange(start: UInt, nItems: UInt) =
		gtk_bitset_add_range(bitSetPointer, start, nItems)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.add_range_closed.html">
	 *     gtk_bitset_add_range_closed</a>
	 */
	fun addRangeClosed(first: UInt, last: UInt) {
		gtk_bitset_add_range_closed(bitSetPointer, first, last)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.add_rectangle.html">
	 *     gtk_bitset_add_rectangle</a>
	 */
	fun addRectangle(start: UInt, width: UInt, height: UInt, stride: UInt) {
		gtk_bitset_add_rectangle(bitSetPointer, start, width, height, stride)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.contains.html">
	 *     gtk_bitset_contains</a>
	 */
	operator fun contains(value: UInt): Boolean =
		gtk_bitset_contains(bitSetPointer, value).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.copy.html">
	 *     gtk_bitset_copy</a>
	 */
	fun copy() =
		gtk_bitset_copy(bitSetPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.difference.html">
	 *     gtk_bitset_difference</a>
	 */
	fun difference(other: KGBitSet) {
		gtk_bitset_difference(bitSetPointer, other.bitSetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.equals.html">
	 *     gtk_bitset_equals</a>
	 */
	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other == null) return false
		if (other !is KGBitSet) return false

		return gtk_bitset_equals(bitSetPointer, other.bitSetPointer).bool
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.get_maximum.html">
	 *     gtk_bitset_get_maximum</a>
	 */
	val maximum: UInt
		get() = gtk_bitset_get_maximum(bitSetPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.get_minimum.html">
	 *     gtk_bitset_get_minimum</a>
	 */
	val minimum: UInt
		get() = gtk_bitset_get_minimum(bitSetPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.get_nth.html">
	 *     gtk_bitset_get_nth</a>
	 */
	fun getNth(index: UInt): UInt =
		gtk_bitset_get_nth(bitSetPointer, index)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.get_size.html">
	 *     gtk_bitset_get_size</a>
	 */
	val size: ULong
		get() = gtk_bitset_get_size(bitSetPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.get_size_in_range.html">
	 *     gtk_bitset_get_size_in_range</a>
	 */
	fun getSizeInRange(first: UInt, last: UInt): ULong =
		gtk_bitset_get_size_in_range(bitSetPointer, first, last)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.intersect.html">
	 *     gtk_bitset_intersect</a>
	 */
	fun intersect(other: KGBitSet) {
		gtk_bitset_intersect(bitSetPointer, other.bitSetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.is_empty.html">
	 *     gtk_bitset_is_empty</a>
	 */
	val isEmpty: Boolean
		get() = gtk_bitset_is_empty(bitSetPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.ref.html">
	 *     gtk_bitset_ref</a>
	 */
	fun ref(): KGBitSet = gtk_bitset_ref(bitSetPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.remove.html">
	 *     gtk_bitset_remove</a>
	 */
	fun remove(value: UInt): Boolean =
		gtk_bitset_remove(bitSetPointer, value).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.remove_all.html">
	 *     gtk_bitset_remove_all</a>
	 */
	fun removeAll() {
		gtk_bitset_remove_all(bitSetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.remove_range.html">
	 *     gtk_bitset_remove_range</a>
	 */
	fun removeRange(start: UInt, count: UInt) {
		gtk_bitset_remove_range(bitSetPointer, start, count)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.remove_range_closed.html">
	 *     gtk_bitset_remove_range_closed</a>
	 */
	fun removeRangeClosed(first: UInt, last: UInt) {
		gtk_bitset_remove_range_closed(bitSetPointer, first, last)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.remove_rectangle.html">
	 *     gtk_bitset_remove_rectangle</a>
	 */
	fun removeRectangle(start: UInt, width: UInt, height: UInt, stride: UInt) {
		gtk_bitset_remove_rectangle(bitSetPointer, start, width, height, stride)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.shift_left.html">
	 *     gtk_bitset_shift_left</a>
	 */
	fun shiftLeft(amount: UInt) {
		gtk_bitset_shift_left(bitSetPointer, amount)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.shift_right.html">
	 *     gtk_bitset_shift_right</a>
	 */
	fun shiftRight(amount: UInt) {
		gtk_bitset_shift_right(bitSetPointer, amount)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.splice.html">
	 *     gtk_bitset_splice</a>
	 */
	fun splice(position: UInt, removed: UInt, added: UInt) {
		gtk_bitset_splice(bitSetPointer, position, removed, added)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.subtract.html">
	 *     gtk_bitset_subtract</a>
	 */
	fun subtract(other: KGBitSet) {
		gtk_bitset_subtract(bitSetPointer, other.bitSetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.union.html">
	 *     gtk_bitset_union</a>
	 */
	fun union(other: KGBitSet) {
		gtk_bitset_union(bitSetPointer, other.bitSetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Bitset.unref.html">
	 *     gtk_bitset_unref</a>
	 */
	override fun unref() {
		gtk_bitset_unref(bitSetPointer)
	}

	companion object {
		inline fun CPointer<GtkBitset>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkBitset>.wrap() =
			KGBitSet(this)
	}

}
