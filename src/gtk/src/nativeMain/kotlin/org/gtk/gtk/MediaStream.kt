package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gdk.Paintable
import org.gtk.gdk.Surface
import org.gtk.glib.KGError
import org.gtk.glib.KGError.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 17 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.MediaStream.html">
 *     GtkMediaStream</a>
 */
class MediaStream(val mediaStreamPointer: GtkMediaStream_autoptr) :
	KGObject(mediaStreamPointer.reinterpret()), Paintable {
	override val paintablePointer: CPointer<GdkPaintable> by lazy {
		mediaStreamPointer.reinterpret()
	}


	fun error(domain: UInt, code: Int, message: String) {
		gtk_media_stream_error(mediaStreamPointer, domain, code, message)
	}

	fun gerror(error: KGError) {
		gtk_media_stream_gerror(mediaStreamPointer, error.pointer)
	}

	val duration: Long
		get() = gtk_media_stream_get_duration(mediaStreamPointer)

	val ended: Boolean
		get() = gtk_media_stream_get_ended(mediaStreamPointer).bool

	val error: KGError?
		get() = gtk_media_stream_get_error(mediaStreamPointer).wrap()

	var loop: Boolean
		get() = gtk_media_stream_get_loop(mediaStreamPointer).bool
		set(value) = gtk_media_stream_set_loop(mediaStreamPointer, value.gtk)

	var muted: Boolean
		get() = gtk_media_stream_get_muted(mediaStreamPointer).bool
		set(value) = gtk_media_stream_set_muted(mediaStreamPointer, value.gtk)

	var playing: Boolean
		get() = gtk_media_stream_get_playing(mediaStreamPointer).bool
		set(value) = gtk_media_stream_set_playing(mediaStreamPointer, value.gtk)

	val timestamp: Long
		get() = gtk_media_stream_get_timestamp(mediaStreamPointer)

	var volume: Double
		get() = gtk_media_stream_get_volume(mediaStreamPointer)
		set(value) = gtk_media_stream_set_volume(mediaStreamPointer, value)

	val hasAudio: Boolean
		get() = gtk_media_stream_has_audio(mediaStreamPointer).bool

	val hasVideo: Boolean
		get() = gtk_media_stream_has_video(mediaStreamPointer).bool

	val isPrepared: Boolean
		get() = gtk_media_stream_is_prepared(mediaStreamPointer).bool

	val isSeekable: Boolean
		get() = gtk_media_stream_is_seekable(mediaStreamPointer).bool

	val isSeeking: Boolean
		get() = gtk_media_stream_is_seeking(mediaStreamPointer).bool

	fun pause() {
		gtk_media_stream_pause(mediaStreamPointer)
	}

	fun play() {
		gtk_media_stream_play(mediaStreamPointer)
	}

	fun realize(surface: Surface) {
		gtk_media_stream_realize(mediaStreamPointer, surface.surfacePointer)
	}

	fun seek(timestamp: Long) {
		gtk_media_stream_seek(mediaStreamPointer, timestamp)
	}

	fun seekFailed() {
		gtk_media_stream_seek_failed(mediaStreamPointer)
	}

	fun seekSuccess() {
		gtk_media_stream_seek_success(mediaStreamPointer)
	}

	fun streamEnded() {
		gtk_media_stream_stream_ended(mediaStreamPointer)
	}

	fun streamPrepared(
		hasAudio: Boolean,
		hasVideo: Boolean,
		seekable: Boolean,
		duration: Long
	) {
		gtk_media_stream_stream_prepared(
			mediaStreamPointer,
			hasAudio.gtk,
			hasVideo.gtk,
			seekable.gtk,
			duration
		)
	}

	fun streamUnprepared() {
		gtk_media_stream_stream_unprepared(mediaStreamPointer)
	}

	fun unrealize(surface: Surface) {
		gtk_media_stream_unrealize(mediaStreamPointer, surface.surfacePointer)
	}

	fun update(timestamp: Long) {
		gtk_media_stream_update(mediaStreamPointer, timestamp)
	}
	
	companion object{

		inline fun CPointer<GtkMediaStream>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkMediaStream>.wrap() =
			MediaStream(this)
	}

}