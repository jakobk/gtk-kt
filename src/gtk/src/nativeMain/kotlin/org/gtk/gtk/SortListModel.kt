package org.gtk.gtk

import gio.GListModel
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gtk.Sorter.Companion.wrap

/**
 * gtk-kt
 *
 * 09 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.SortListModel.html">
 *     GtkSortListModel</a>
 */
class SortListModel(val sortListModelPointer: CPointer<GtkSortListModel>) :
	KGObject(sortListModelPointer.reinterpret()), ListModel {

	override val listModelPointer: CPointer<GListModel> by lazy { sortListModelPointer.reinterpret() }

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.SortListModel.new.html">
	 *     gtk_sort_list_model_new</a>
	 */
	constructor(model: ListModel?, sorter: Sorter?) : this(
		gtk_sort_list_model_new(
			model?.listModelPointer,
			sorter?.sorterPointer
		)!!
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SortListModel.get_incremental.html">
	 *     gtk_sort_list_model_get_incremental</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.SortListModel.set_incremental.html">
	 *     gtk_sort_list_model_set_incremental</a>
	 */
	var incremental: Boolean
		get() = gtk_sort_list_model_get_incremental(sortListModelPointer).bool
		set(value) = gtk_sort_list_model_set_incremental(sortListModelPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SortListModel.get_model.html">
	 *     gtk_sort_list_model_get_model</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.SortListModel.set_model.html">
	 *     gtk_sort_list_model_set_model</a>
	 */
	var model: ListModel?
		get() = gtk_sort_list_model_get_model(sortListModelPointer).wrap()
		set(value) = gtk_sort_list_model_set_model(sortListModelPointer, value?.listModelPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SortListModel.get_pending.html">
	 *     gtk_sort_list_model_get_pending</a>
	 */
	val pending: UInt
		get() = gtk_sort_list_model_get_pending(sortListModelPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SortListModel.get_sorter.html">
	 *     gtk_sort_list_model_get_sorter</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.SortListModel.set_sorter.html">
	 *     gtk_sort_list_model_set_sorter</a>
	 */
	var sorter: Sorter?
		get() = gtk_sort_list_model_get_sorter(sortListModelPointer).wrap()
		set(value) = gtk_sort_list_model_set_sorter(sortListModelPointer, value?.sorterPointer)

}