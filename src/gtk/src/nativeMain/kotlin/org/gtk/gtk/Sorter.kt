package org.gtk.gtk

import glib.gpointer
import gobject.GCallback
import gtk.*
import gtk.GtkSorterChange.*
import gtk.GtkSorterOrder.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gtk.widgets.Widget

/**
 * gtk-kt
 *
 * 09 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Sorter.html">GtkSorter</a>
 */
class Sorter(val sorterPointer: CPointer<GtkSorter>) : KGObject(sorterPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Sorter.changed.html">gtk_sorter_changed</a>
	 */
	fun changed(change: Change) {
		gtk_sorter_changed(sorterPointer, change.gtk)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Sorter.compare.html">gtk_sorter_compare</a>
	 */
	fun compare(item1: KGObject, item2: KGObject): Ordering =
		Ordering.valueOf(gtk_sorter_compare(sorterPointer, item1.pointer, item2.pointer))!!

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Sorter.get_order.html">gtk_sorter_get_order</a>
	 */
	val order: Order
		get() = Order.valueOf(gtk_sorter_get_order(sorterPointer))!!

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Sorter.changed.html">changed</a>
	 */
	fun addOnChangedCallback(action: (Change) -> Unit) =
		addSignalCallback(Signals.CHANGED, action, staticChangedFunction)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.SorterChange.html">GtkSorterChange</a>
	 */
	enum class Change(val gtk: GtkSorterChange) {
		DIFFERENT(GTK_SORTER_CHANGE_DIFFERENT),
		INVERTED(GTK_SORTER_CHANGE_INVERTED),
		LESS_STRICT(GTK_SORTER_CHANGE_LESS_STRICT),
		MORE_STRICT(GTK_SORTER_CHANGE_MORE_STRICT);

		companion object {
			inline fun valueOf(gtk: GtkSorterChange) = values().find { it.gtk == gtk }
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.SorterOrder.html">GtkSorterOrder</a>
	 */
	enum class Order(val gtk: GtkSorterOrder) {
		PARTIAL(GTK_SORTER_ORDER_PARTIAL),
		NONE(GTK_SORTER_ORDER_NONE),
		TOTAL(GTK_SORTER_ORDER_TOTAL);

		companion object {
			inline fun valueOf(gtk: GtkSorterOrder) = values().find { it.gtk == gtk }
		}
	}

	companion object {
		private val staticChangedFunction: GCallback =
			staticCFunction { _: gpointer, change: GtkSorterChange, data: gpointer ->
				data.asStableRef<(Change) -> Unit>().get().invoke(Change.valueOf(change)!!)
				Unit
			}.reinterpret()

		inline fun CPointer<GtkSorter>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkSorter>.wrap() =
			Sorter(this)
	}
}