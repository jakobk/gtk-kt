package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.Widget.Companion.wrap

/**
 * gtk-kt
 *
 * 09 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.StackPage.html">
 *     GtkStackPage</a>
 */
class StackPage(val stackPagePointer: CPointer<GtkStackPage>) : KGObject(stackPagePointer.reinterpret()), Accessible {
	override val accessiblePointer: CPointer<GtkAccessible> by lazy { stackPagePointer.reinterpret() }

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.get_child.html">
	 *     gtk_stack_page_get_child</a>
	 */
	val child: Widget
		get() = gtk_stack_page_get_child(stackPagePointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.get_icon_name.html">
	 *     gtk_stack_page_get_icon_name</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.set_icon_name.html">
	 *     gtk_stack_page_set_icon_name</a>
	 */
	var iconName: String?
		get() = gtk_stack_page_get_icon_name(stackPagePointer)?.toKString()
		set(value) = gtk_stack_page_set_icon_name(stackPagePointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.get_name.html">
	 *     gtk_stack_page_get_name</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.set_name.html">
	 *     gtk_stack_page_set_name</a>
	 */
	var name: String?
		get() = gtk_stack_page_get_name(stackPagePointer)?.toKString()
		set(value) = gtk_stack_page_set_name(stackPagePointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.get_needs_attention.html">
	 *     gtk_stack_page_get_needs_attention</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.set_needs_attention.html">
	 *     gtk_stack_page_set_needs_attention</a>
	 */
	var needAttention: Boolean
		get() = gtk_stack_page_get_needs_attention(stackPagePointer).bool
		set(value) = gtk_stack_page_set_needs_attention(stackPagePointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.get_title.html">
	 *     gtk_stack_page_get_title</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.set_title.html">
	 *     gtk_stack_page_set_title</a>
	 */
	var title: String?
		get() = gtk_stack_page_get_title(stackPagePointer)?.toKString()
		set(value) = gtk_stack_page_set_title(stackPagePointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.get_use_underline.html">
	 *     gtk_stack_page_get_use_underline</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.set_use_underline.html">
	 *     gtk_stack_page_set_use_underline</a>
	 */
	var useUnderline: Boolean
		get() = gtk_stack_page_get_use_underline(stackPagePointer).bool
		set(value) = gtk_stack_page_set_use_underline(stackPagePointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.get_visible.html">
	 *     gtk_stack_page_get_visible</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.StackPage.set_visible.html">
	 *     gtk_stack_page_set_visible</a>
	 */
	var visible: Boolean
		get() = gtk_stack_page_get_visible(stackPagePointer).bool
		set(value) = gtk_stack_page_set_visible(stackPagePointer, value.gtk)
}