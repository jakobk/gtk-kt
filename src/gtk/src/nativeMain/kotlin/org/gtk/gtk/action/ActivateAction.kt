package org.gtk.gtk.action

import gtk.GtkActivateAction
import gtk.GtkActivateAction_autoptr
import gtk.gtk_activate_action_get
import kotlinx.cinterop.reinterpret

/**
 * gtk-kt
 *
 * 19 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ActivateAction.html">
 *     GtkActivateAction</a>
 */
class ActivateAction(val activateAction: GtkActivateAction_autoptr) :
	ShortcutAction(activateAction.reinterpret()) {

	companion object {

		fun get(): ActivateAction =
			gtk_activate_action_get()!!.reinterpret<GtkActivateAction>().wrap()

		inline fun GtkActivateAction_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkActivateAction_autoptr.wrap() =
			ActivateAction(this)
	}
}