package org.gtk.gtk.action

import gtk.GtkMnemonicAction
import gtk.GtkMnemonicAction_autoptr
import gtk.gtk_mnemonic_action_get
import kotlinx.cinterop.reinterpret

/**
 * gtk-kt
 *
 * 20 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.MnemonicAction.html">
 *     GtkMnemonicAction</a>
 */
class MnemonicAction(val mnemonicAction: GtkMnemonicAction_autoptr) :
	ShortcutAction(mnemonicAction.reinterpret()) {
	companion object {

		fun get(): MnemonicAction =
			gtk_mnemonic_action_get()!!.reinterpret<GtkMnemonicAction>().wrap()

		inline fun GtkMnemonicAction_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkMnemonicAction_autoptr.wrap() =
			MnemonicAction(this)
	}
}