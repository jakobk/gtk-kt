package org.gtk.gtk.action

import gtk.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

/**
 * gtk-kt
 *
 * 20 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.NamedAction.html">
 *     GtkNamedAction</a>
 */
class NamedAction(val namedAction: GtkNamedAction_autoptr) :
	ShortcutAction(namedAction.reinterpret()) {

	constructor(name: String) : this(gtk_named_action_new(name)!!.reinterpret())

	val actionName: String
		get() = gtk_named_action_get_action_name(namedAction)!!.toKString()

	companion object {

		inline fun GtkNamedAction_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkNamedAction_autoptr.wrap() =
			NamedAction(this)
	}
}