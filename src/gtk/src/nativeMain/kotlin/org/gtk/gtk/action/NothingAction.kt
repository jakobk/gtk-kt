package org.gtk.gtk.action

import gtk.GtkNothingAction
import gtk.GtkNothingAction_autoptr
import gtk.gtk_nothing_action_get
import kotlinx.cinterop.reinterpret

/**
 * gtk-kt
 *
 * 20 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.NothingAction.html">
 *     GtkNothingAction</a>
 */
class NothingAction(val mnemonicAction: GtkNothingAction_autoptr) :
	ShortcutAction(mnemonicAction.reinterpret()) {
	companion object {

		fun get(): NothingAction =
			gtk_nothing_action_get()!!.reinterpret<GtkNothingAction>().wrap()

		inline fun GtkNothingAction_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkNothingAction_autoptr.wrap() =
			NothingAction(this)
	}
}