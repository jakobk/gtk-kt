package org.gtk.gtk.action

import gtk.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.Variant
import org.gtk.glib.bool
import org.gtk.gobject.KGObject
import org.gtk.gtk.widgets.Widget

/**
 * gtk-kt
 *
 * 19 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutAction.html">
 *     GtkShortcutAction</a>
 */
open class ShortcutAction(val shortcutAction: GtkShortcutAction_autoptr) :
	KGObject(shortcutAction.reinterpret()) {

	constructor(string: String) : this(gtk_shortcut_action_parse_string(string)!!)

	fun activate(flags: Flags, widget: Widget, args: Variant) =
		gtk_shortcut_action_activate(
			shortcutAction,
			flags.gtk,
			widget.widgetPointer,
			args.variantPointer
		).bool


	override fun toString(): String =
		gtk_shortcut_action_to_string(shortcutAction)!!.toKString()

	/**
	 * gtk-kt
	 *
	 * 19 / 11 / 2021
	 *
	 * @see <a href="https://docs.gtk.org/gtk4/flags.ShortcutActionFlags.html">
	 *     GtkShortcutActionFlags</a>
	 */
	enum class Flags(val gtk: GtkShortcutActionFlags) {
		EXCLUSIVE(GTK_SHORTCUT_ACTION_EXCLUSIVE);

		companion object {
			fun valueOf(gtk: GtkShortcutActionFlags) =
				when (gtk) {
					GTK_SHORTCUT_ACTION_EXCLUSIVE -> EXCLUSIVE
					else -> EXCLUSIVE
				}
		}
	}
}