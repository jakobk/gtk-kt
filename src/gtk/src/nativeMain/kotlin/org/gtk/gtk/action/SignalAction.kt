package org.gtk.gtk.action

import gtk.GtkSignalAction_autoptr
import gtk.gtk_signal_action_get_signal_name
import gtk.gtk_signal_action_new
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

/**
 * gtk-kt
 *
 * 20 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.SignalAction.html">
 *     GtkSignalAction</a>
 */
class SignalAction(val namedAction: GtkSignalAction_autoptr) :
	ShortcutAction(namedAction.reinterpret()) {

	constructor(signalName: String) :
			this(gtk_signal_action_new(signalName)!!.reinterpret())

	val signalName: String
		get() = gtk_signal_action_get_signal_name(namedAction)!!.toKString()

	companion object {

		inline fun GtkSignalAction_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkSignalAction_autoptr.wrap() =
			SignalAction(this)
	}
}