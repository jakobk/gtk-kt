package org.gtk.gtk.common.callback

import glib.gpointer
import gobject.GCallback
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.gtk

/*
 * gtk-kt
 *
 * 18 / 11 / 2021
 */
typealias ChangeCurrentPageFunction = (Int) -> Boolean

val staticChangeCurrentPageFunction: GCallback =
	staticCFunction { _: gpointer?, arg1: Int, data: gpointer? ->
		data?.asStableRef<ChangeCurrentPageFunction>()
			?.get()
			?.invoke(arg1).gtk
	}.reinterpret()