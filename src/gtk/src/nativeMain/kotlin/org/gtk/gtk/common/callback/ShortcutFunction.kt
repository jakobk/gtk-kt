package org.gtk.gtk.common.callback

import glib.GVariant_autoptr
import glib.gpointer
import gtk.GtkShortcutFunc
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.Variant
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.glib.gtk

typealias ShortcutFunction = (args: Variant) -> Boolean

val staticShortcutFunction: GtkShortcutFunc =
	staticCFunction { _, args: GVariant_autoptr?, data: gpointer? ->
		data?.asStableRef<ShortcutFunction>()
			?.get()?.let {
				if (args == null) null else it.invoke(args.wrap()).gtk
			} ?: false.gtk
	}