package org.gtk.gtk.filter

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gtk.Expression
import org.gtk.gtk.Expression.Companion.wrap

/**
 * gtk-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href=""></a>
 */
class BoolFilter(val boolFilterPointer: GtkBoolFilter_autoptr) :
	Filter(boolFilterPointer.reinterpret()) {

	constructor(expression: Expression?) :
			this(gtk_bool_filter_new(expression?.expressionPointer)!!)

	var expression: Expression?
		get() = gtk_bool_filter_get_expression(boolFilterPointer).wrap()
		set(value) = gtk_bool_filter_set_expression(boolFilterPointer, value?.expressionPointer)

	var invert: Boolean
		get() = gtk_bool_filter_get_invert(boolFilterPointer).bool
		set(value) = gtk_bool_filter_set_invert(boolFilterPointer, value.gtk)
}