package org.gtk.gtk.filter

import glib.gpointer
import gobject.GObject
import gtk.GtkCustomFilterFunc
import gtk.GtkCustomFilter_autoptr
import gtk.gtk_custom_filter_new
import gtk.gtk_custom_filter_set_filter_func
import kotlinx.cinterop.StableRef
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gobject.staticDestroyStableRefFunction

/**
 * gtk-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CustomFilter.html">
 *     GtkCustomFilter</a>
 */
class CustomFilter(val customFilterPointer: GtkCustomFilter_autoptr) :
	Filter(customFilterPointer.reinterpret()) {

	constructor(matchFunction: CustomFilterFunction) :
			this(
				gtk_custom_filter_new(
					staticCustomFilterFunction,
					StableRef.create(matchFunction).asCPointer(),
					staticDestroyStableRefFunction
				)!!
			)

	fun setFilterFunction(matchFunction: CustomFilterFunction) {
		gtk_custom_filter_set_filter_func(
			customFilterPointer,
			staticCustomFilterFunction,
			StableRef.create(matchFunction).asCPointer(),
			staticDestroyStableRefFunction
		)
	}

	companion object {
		private val staticCustomFilterFunction: GtkCustomFilterFunc =
			staticCFunction { item: gpointer?, data: gpointer? ->
				data?.asStableRef<CustomFilterFunction>()?.get()
					?.invoke(item!!.reinterpret<GObject>().wrap()).gtk
			}
	}
}

/**
 * @see <a href="https://docs.gtk.org/gtk4/callback.CustomFilterFunc.html">
 *     GtkCustomFilterFunc</a>
 */
typealias CustomFilterFunction = (item: KGObject) -> Boolean