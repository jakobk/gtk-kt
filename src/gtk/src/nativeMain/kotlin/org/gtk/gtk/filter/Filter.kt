package org.gtk.gtk.filter

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.bool
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback

/**
 * gtk-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Filter.html">GtkFilter</a>
 */
open class Filter(val filterPointer: GtkFilter_autoptr) :
	KGObject(filterPointer.reinterpret()) {

	fun changed(change: Change) {
		gtk_filter_changed(filterPointer, change.gtk)
	}

	val strictness: Match
		get() = Match.valueOf(gtk_filter_get_strictness(filterPointer))

	fun match(item: KGObject): Boolean =
		gtk_filter_match(filterPointer, item.pointer).bool

	fun addOnChangedCallback(action: FilterChangedFunction) =
		addSignalCallback(Signals.CHANGED, action, staticFilterChangedFunction)

	/**
	 * gtk-kt
	 *
	 * 21 / 11 / 2021
	 *
	 * @see <a href="https://docs.gtk.org/gtk4/enum.FilterChange.html">
	 *     GtkFilterChange</a>
	 */
	enum class Change(val gtk: GtkFilterChange) {
		DIFFERENT(GTK_FILTER_CHANGE_DIFFERENT),
		LESS_STRICT(GTK_FILTER_CHANGE_LESS_STRICT),
		MORE_STRICT(GTK_FILTER_CHANGE_MORE_STRICT);

		companion object {

			fun valueOf(gtk: GtkFilterChange): Change =
				when (gtk) {
					GTK_FILTER_CHANGE_LESS_STRICT -> LESS_STRICT
					GTK_FILTER_CHANGE_MORE_STRICT -> MORE_STRICT
					else -> DIFFERENT
				}
		}
	}

	/**
	 * gtk-kt
	 *
	 * 21 / 11 / 2021
	 *
	 * @see <a href="https://docs.gtk.org/gtk4/enum.FilterMatch.html">
	 *     GtkFilterMatch</a>
	 */
	enum class Match(val gtk: GtkFilterMatch) {
		SOME(GTK_FILTER_MATCH_SOME),
		NONE(GTK_FILTER_MATCH_NONE),
		ALL(GTK_FILTER_MATCH_ALL);

		companion object {

			fun valueOf(gtk: GtkFilterMatch): Match =
				when (gtk) {
					GTK_FILTER_CHANGE_LESS_STRICT -> SOME
					GTK_FILTER_CHANGE_MORE_STRICT -> NONE
					else -> ALL
				}
		}
	}

	companion object {
		private val staticFilterChangedFunction: GCallback =
			staticCFunction { _: gpointer, change: GtkFilterChange, data: gpointer ->
				data.asStableRef<FilterChangedFunction>().get().invoke(Change.valueOf(change))
				Unit
			}.reinterpret()
	}
}

typealias FilterChangedFunction = (change: Filter.Change) -> Unit