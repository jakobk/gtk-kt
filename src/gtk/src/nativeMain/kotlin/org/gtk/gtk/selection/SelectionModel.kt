package org.gtk.gtk.selection

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gio.ListModel
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gtk.KGBitSet
import org.gtk.gtk.KGBitSet.Companion.wrap

/**
 * gtk-kt
 *
 * 2 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/iface.SelectionModel.html">
 *     GtkSelectionModel</a>
 */
interface SelectionModel : ListModel {
	val selectionModelPointer: GtkSelectionModel_autoptr

	val selection: KGBitSet
		get() = gtk_selection_model_get_selection(selectionModelPointer)!!.wrap()

	fun getSelectionInRange(position: UInt, nItems: UInt): KGBitSet =
		gtk_selection_model_get_selection_in_range(
			selectionModelPointer,
			position,
			nItems
		)!!.wrap()

	fun isSelected(position: UInt): Boolean =
		gtk_selection_model_is_selected(selectionModelPointer, position).bool

	fun selectAll(): Boolean =
		gtk_selection_model_select_all(selectionModelPointer).bool

	fun selectItem(position: UInt, unselectRest: Boolean): Boolean =
		gtk_selection_model_select_item(
			selectionModelPointer,
			position,
			unselectRest.gtk
		).bool

	fun selectRange(
		position: UInt,
		nItems: UInt,
		unselectRest: Boolean
	): Boolean =
		gtk_selection_model_select_range(
			selectionModelPointer,
			position,
			nItems,
			unselectRest.gtk
		).bool

	fun selectionChanged(position: UInt, nItems: UInt) {
		gtk_selection_model_selection_changed(
			selectionModelPointer,
			position,
			nItems
		)
	}

	fun setSelection(selected: KGBitSet, mask: KGBitSet): Boolean =
		gtk_selection_model_set_selection(
			selectionModelPointer,
			selected.bitSetPointer,
			mask.bitSetPointer
		).bool

	fun unselectAll(): Boolean =
		gtk_selection_model_unselect_all(selectionModelPointer).bool

	fun unselectItem(position: UInt): Boolean =
		gtk_selection_model_unselect_item(selectionModelPointer, position).bool

	fun unselectRange(position: UInt, nItems: UInt): Boolean =
		gtk_selection_model_unselect_range(
			selectionModelPointer,
			position,
			nItems
		).bool

	fun addOnSelectionChangedCallback(action: SelectionChangedFunction) =
		KGObject(selectionModelPointer.reinterpret()).addSignalCallback(
			Signals.SELECTION_CHANGED,
			action,
			staticSelectionChangedFunc
		)

	companion object {
		private val staticSelectionChangedFunc: GCallback =
			staticCFunction { _: gpointer,
			                  position: UInt,
			                  nItems: UInt,
			                  data: gpointer ->
				data.asStableRef<SelectionChangedFunction>()
					.get().invoke(position, nItems)
			}.reinterpret()
	}
}

typealias SelectionChangedFunction = (position: UInt, nItems: UInt) -> Unit
