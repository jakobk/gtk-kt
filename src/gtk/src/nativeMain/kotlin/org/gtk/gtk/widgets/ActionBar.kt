package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject
import org.gtk.gtk.Accessible
import org.gtk.gtk.Buildable
import org.gtk.gtk.ConstraintTarget

/**
 * kotlinx-gtk
 *
 * 13 / 03 / 2021
 *
 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkActionBar.html">GtkActionBar</a>
 */
open class ActionBar(
	val actionBarPointer: CPointer<GtkActionBar>
) : Widget(actionBarPointer.reinterpret()), Accessible, Buildable, ConstraintTarget {

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkActionBar.html#gtk-action-bar-get-center-widget">
	 *     gtk_action_bar_get_center_widget</a>
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkActionBar.html#gtk-action-bar-set-center-widget">
	 *     gtk_action_bar_set_center_widget</a>
	 */
	var centerWidget: Widget?
		get() = gtk_action_bar_get_center_widget(actionBarPointer)?.let {
			Widget(it)
		}
		set(value) = gtk_action_bar_set_center_widget(
			actionBarPointer,
			value?.widgetPointer
		)

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkActionBar.html#gtk-action-bar-new">
	 *     gtk_action_bar_new</a>
	 */
	constructor() : this(gtk_action_bar_new()!!.reinterpret())

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkActionBar.html#gtk-action-bar-pack-start">
	 *     gtk_action_bar_pack_start</a>
	 */
	fun packStart(child: Widget) {
		gtk_action_bar_pack_start(actionBarPointer, child.widgetPointer)
	}

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkActionBar.html#gtk-action-bar-pack-end">
	 *     gtk_action_bar_pack_end</a>
	 */
	fun packEnd(child: Widget) {
		gtk_action_bar_pack_end(actionBarPointer, child.widgetPointer)
	}

	companion object{

		fun CPointer<GtkActionBar>?.wrap() =
			this?.wrap()

		fun CPointer<GtkActionBar>.wrap() =
			ActionBar(this)
	}
}