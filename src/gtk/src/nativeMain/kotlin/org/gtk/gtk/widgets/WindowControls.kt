package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.bool
import org.gtk.gtk.common.enums.PackType

/**
 * gtk-kt
 *
 * 18 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.WindowControls.html#description">
 *     GtkWindowControls</a>
 */
class WindowControls(val windowControlsPointer: CPointer<GtkWindowControls>) :
	Widget(windowControlsPointer.reinterpret()) {

	constructor(side: PackType) :
			this(gtk_window_controls_new(side.gtk)!!.reinterpret())

	var decorationLayout: String?
		get() = gtk_window_controls_get_decoration_layout(windowControlsPointer)?.toKString()
		set(value) = gtk_window_controls_set_decoration_layout(windowControlsPointer, value)

	val empty: Boolean
		get() = gtk_window_controls_get_empty(windowControlsPointer).bool

	var side: PackType
		get() = PackType.valueOf(gtk_window_controls_get_side(windowControlsPointer))
		set(value) = gtk_window_controls_set_side(windowControlsPointer, value.gtk)
}