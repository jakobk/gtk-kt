package org.gtk.gtk.widgets.box

import gtk.GtkShortcutsSection
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gtk.common.callback.ChangeCurrentPageFunction
import org.gtk.gtk.common.callback.staticChangeCurrentPageFunction

/**
 * gtk-kt
 *
 * 18 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutsSection.html">
 *     GtkShortcutsSection</a>
 */
class ShortcutsSection(
	val shortcutsSectionPointer: CPointer<GtkShortcutsSection>
) : Box(shortcutsSectionPointer.reinterpret()) {

	fun addOnChangeCurrentPageFunction(action: ChangeCurrentPageFunction) =
		addSignalCallback(
			Signals.CHANGE_CURRENT_PAGE,
			action,
			staticChangeCurrentPageFunction
		)
}