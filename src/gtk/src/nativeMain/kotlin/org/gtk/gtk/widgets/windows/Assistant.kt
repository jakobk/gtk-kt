package org.gtk.gtk.widgets.windows

import glib.gpointer
import gtk.*
import gtk.GtkAssistantPageType.*
import kotlinx.cinterop.*
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gobject.staticDestroyStableRefFunction
import org.gtk.gtk.AssistantPageFunction
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.windows.Assistant.Page.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 08 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Assistant.html">
 *     GtkAssistant</a>
 */
class Assistant(val assistantPointer: CPointer<GtkAssistant>) :
	Window(assistantPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Assistant.new.html">
	 *     gtk_assistant_new</a>
	 */
	constructor() : this(gtk_assistant_new()!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.add_action_widget.html">
	 *     gtk_assistant_add_action_widget</a>
	 */
	fun addActionWidget(child: Widget) {
		gtk_assistant_add_action_widget(assistantPointer, child.widgetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.append_page.html">
	 *     gtk_assistant_append_page</a>
	 */
	fun appendPage(page: Widget): Int =
		gtk_assistant_append_page(assistantPointer, page.widgetPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.commit.html">
	 *     gtk_assistant_commit</a>
	 */
	fun commit() {
		gtk_assistant_commit(assistantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.get_current_page.html">
	 *     gtk_assistant_get_current_page</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.set_current_page.html">
	 *     gtk_assistant_set_current_page</a>
	 */
	var currentPage: Int
		get() = gtk_assistant_get_current_page(assistantPointer)
		set(value) = gtk_assistant_set_current_page(assistantPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.get_n_pages.html">
	 *     gtk_assistant_get_n_pages</a>
	 */
	val nPages: Int
		get() = gtk_assistant_get_n_pages(assistantPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.get_nth_page.html">
	 *     gtk_assistant_get_nth_page</a>
	 */
	fun getNthPage(pageNum: Int): Widget? =
		gtk_assistant_get_nth_page(assistantPointer, pageNum).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.get_page.html">
	 *     gtk_assistant_get_page</a>
	 */
	fun page(child: Widget): Page =
		gtk_assistant_get_page(assistantPointer, child.widgetPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.get_page_complete.html">
	 *     gtk_assistant_get_page_complete</a>
	 */
	fun getPageComplete(page: Widget): Boolean =
		gtk_assistant_get_page_complete(assistantPointer, page.widgetPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.get_page_title.html">
	 *     gtk_assistant_get_page_title</a>
	 */
	fun getPageTitle(page: Widget): String =
		gtk_assistant_get_page_title(assistantPointer, page.widgetPointer)!!.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.get_page_type.html">
	 *     gtk_assistant_get_page_type</a>
	 */
	fun getPageType(page: Widget): Page.Type =
		Page.Type.valueOf(
			gtk_assistant_get_page_type(
				assistantPointer,
				page.widgetPointer
			)
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.get_pages.html">
	 *     gtk_assistant_get_pages</a>
	 */
	fun getPages(): ListModel =
		gtk_assistant_get_pages(assistantPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.insert_page.html">
	 *     gtk_assistant_insert_page</a>
	 */
	fun insertPage(page: Widget, position: Int): Int =
		gtk_assistant_insert_page(assistantPointer, page.widgetPointer, position)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.next_page.html">
	 *     gtk_assistant_next_page</a>
	 */
	fun nextPage() {
		gtk_assistant_next_page(assistantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.prepend_page.html">
	 *     gtk_assistant_prepend_page</a>
	 */
	fun prependPage(page: Widget): Int =
		gtk_assistant_prepend_page(assistantPointer, page.widgetPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.previous_page.html">
	 *     gtk_assistant_previous_page</a>
	 */
	fun previousPage() {
		gtk_assistant_previous_page(assistantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.remove_action_widget.html">
	 *     gtk_assistant_remove_action_widget</a>
	 */
	fun removeActionWidget(child: Widget) {
		gtk_assistant_remove_action_widget(assistantPointer, child.widgetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.remove_page.html">
	 *     gtk_assistant_remove_page</a>
	 */
	fun removePage(pageNum: Int) {
		gtk_assistant_remove_page(assistantPointer, pageNum)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.set_forward_page_func.html">
	 *     gtk_assistant_set_forward_page_func</a>
	 */
	fun setForwardPageFunction(func: AssistantPageFunction?) {
		if (func != null) {
			gtk_assistant_set_forward_page_func(
				assistantPointer,
				staticAssistantPageFunction,
				StableRef.create(func).asCPointer(),
				staticDestroyStableRefFunction
			)
		} else {
			gtk_assistant_set_forward_page_func(
				assistantPointer,
				null,
				null,
				null
			)
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.set_page_complete.html">
	 *     gtk_assistant_set_page_complete</a>
	 */
	fun setPageComplete(page: Widget, complete: Boolean) {
		gtk_assistant_set_page_complete(
			assistantPointer,
			page.widgetPointer,
			complete.gtk
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.set_page_title.html">
	 *     gtk_assistant_set_page_title</a>
	 */
	fun setPageTitle(page: Widget, title: String) {
		gtk_assistant_set_page_title(
			assistantPointer,
			page.widgetPointer,
			title
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.set_page_type.html">
	 *     gtk_assistant_set_page_type</a>
	 */
	fun setPageType(page: Widget, type: Page.Type) {
		gtk_assistant_set_page_type(
			assistantPointer,
			page.widgetPointer,
			type.gtk
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Assistant.update_buttons_state.html">
	 *     gtk_assistant_update_buttons_state</a>
	 */
	fun updateButtonsState() {
		gtk_assistant_update_buttons_state(assistantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/class.AssistantPage.html">
	 *     GtkAssistantPage</a>
	 */
	class Page(val pagePointer: CPointer<GtkAssistantPage>) :
		KGObject(pagePointer.reinterpret()) {

		/**
		 * @see <a href="https://docs.gtk.org/gtk4/method.AssistantPage.get_child.html">
		 *     gtk_assistant_page_get_child</a>
		 */
		val child: Widget
			get() = gtk_assistant_page_get_child(pagePointer)!!.wrap()

		/**
		 * @see <a href="https://docs.gtk.org/gtk4/enum.AssistantPageType.html">
		 *     GtkAssistantPageType</a>
		 */
		enum class Type(val gtk: GtkAssistantPageType) {
			/**
			 * The page has regular contents.
			 *
			 * Both the Back and forward buttons will be shown.
			 */
			CONTENT(GTK_ASSISTANT_PAGE_CUSTOM),

			/**
			 * The page contains an introduction to the assistant task.
			 *
			 * Only the Forward button will be shown if there is a next page.
			 */
			INTRO(GTK_ASSISTANT_PAGE_INTRO),

			/**
			 * The page lets the user confirm or deny the changes.
			 *
			 * The Back and Apply buttons will be shown.
			 */
			CONFIRM(GTK_ASSISTANT_PAGE_CONFIRM),

			/**
			 * The page informs the user of the changes done.
			 *
			 * Only the Close button will be shown.
			 */
			SUMMARY(GTK_ASSISTANT_PAGE_SUMMARY),

			/**
			 * Used for tasks that take a long time to complete,
			 * blocks the assistant until the page is marked as complete.
			 *
			 * Only the back button will be shown.
			 */
			PROGRESS(GTK_ASSISTANT_PAGE_PROGRESS),

			/**
			 * Used for when other page types are not appropriate.
			 *
			 * No buttons will be shown,
			 * and the application must add its own buttons through
			 * [Assistant.addActionWidget].
			 */
			CUSTOM(GTK_ASSISTANT_PAGE_CUSTOM);

			companion object {
				fun valueOf(gtk: GtkAssistantPageType) =
					when (gtk) {
						GTK_ASSISTANT_PAGE_CONTENT -> CONTENT
						GTK_ASSISTANT_PAGE_INTRO -> INTRO
						GTK_ASSISTANT_PAGE_CONFIRM -> CONFIRM
						GTK_ASSISTANT_PAGE_SUMMARY -> SUMMARY
						GTK_ASSISTANT_PAGE_PROGRESS -> PROGRESS
						GTK_ASSISTANT_PAGE_CUSTOM -> CUSTOM
					}
			}
		}

		companion object {
			inline fun CPointer<GtkAssistantPage>?.wrap() =
				this?.wrap()

			inline fun CPointer<GtkAssistantPage>.wrap() =
				Page(this)
		}
	}

	companion object {
		private val staticAssistantPageFunction: GtkAssistantPageFunc =
			staticCFunction { currentPage: Int, data: gpointer? ->
				data?.asStableRef<AssistantPageFunction>()?.get()?.invoke(currentPage) ?: -1
			}
	}
}