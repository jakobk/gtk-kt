package org.gtk.gtk.widgets.windows

import gtk.GtkShortcutsWindow
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback

/**
 * kotlinx-gtk
 * 07 / 03 / 2021
 */
class ShortcutsWindow(
	@Suppress("MemberVisibilityCanBePrivate")
	val shortCutsWindowPointer: CPointer<GtkShortcutsWindow>
) : Window(shortCutsWindowPointer.reinterpret()) {

	fun addOnCloseCallback(action: () -> Unit) =
		addSignalCallback(Signals.CLOSE, action)

	fun addOnSearchCallback(action: () -> Unit) =
		addSignalCallback(Signals.SEARCH, action)

	companion object {
		inline fun CPointer<GtkShortcutsWindow>.wrap() = ShortcutsWindow(this)
		inline fun CPointer<GtkShortcutsWindow>?.wrap() = this?.wrap()
	}
}