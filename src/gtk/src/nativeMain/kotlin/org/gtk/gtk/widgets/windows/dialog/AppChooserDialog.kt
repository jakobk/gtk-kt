package org.gtk.gtk.widgets.windows.dialog

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gio.File
import org.gtk.gtk.*
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.windows.Window

/**
 * kotlinx-gtk
 *
 * 08 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.AppChooserDialog.html">
 *     GtkAppChooserDialog</a>
 */
class AppChooserDialog(val aboutDialogPointer: CPointer<GtkAppChooserDialog>) :
	Dialog(aboutDialogPointer.reinterpret()),
	Accessible,
	AppChooser,
	Buildable,
	ConstraintTarget,
	Native,
	Root,
	ShortcutManager {

	override val appChooserPointer: GtkAppChooser_autoptr by lazy {
		aboutDialogPointer.reinterpret()
	}


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.AppChooserDialog.new.html">
	 *     gtk_app_chooser_dialog_new</a>
	 */
	constructor(window: Window?, dialogFlags: Flags, file: File) : this(
		gtk_app_chooser_dialog_new(
			window?.windowPointer,
			dialogFlags.gtk,
			file.filePointer
		)!!.reinterpret()
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.AppChooserDialog.new_for_content_type.html">
	 *     gtk_app_chooser_dialog_new_for_content_type</a>
	 */
	constructor(window: Window?, dialogFlags: Flags, contentType: String) : this(
		gtk_app_chooser_dialog_new_for_content_type(
			window?.windowPointer,
			dialogFlags.gtk,
			contentType
		)!!.reinterpret()
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserDialog.get_widget.html">
	 *     gtk_app_chooser_dialog_get_widget</a>
	 */
	val widget: Widget?
		get() = gtk_app_chooser_dialog_get_widget(aboutDialogPointer)?.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserDialog.get_heading.html">
	 *     gtk_app_chooser_dialog_get_heading</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.AppChooserDialog.set_heading.html">
	 *     gtk_app_chooser_dialog_set_heading</a>
	 */
	var heading: String?
		get() =
			gtk_app_chooser_dialog_get_heading(aboutDialogPointer)?.toKString()
		set(value) =
			gtk_app_chooser_dialog_set_heading(aboutDialogPointer, value)

}