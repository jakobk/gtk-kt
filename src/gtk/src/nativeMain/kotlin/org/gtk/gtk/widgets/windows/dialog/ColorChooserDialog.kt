package org.gtk.gtk.widgets.windows.dialog

import gtk.GtkBuildable
import gtk.GtkColorChooser
import gtk.GtkDialog
import gtk.gtk_color_chooser_dialog_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gtk.*
import org.gtk.gtk.widgets.windows.Window

/**
 * kotlinx-gtk
 *
 * 08 / 03 / 2021
 *
 * @see <a href=""></a>
 */
class ColorChooserDialog(val aboutDialogPointer: CPointer<GtkDialog>) :
	Dialog(aboutDialogPointer.reinterpret()),
	Accessible,
	Buildable,
	ColorChooser,
	ConstraintTarget,
	Native,
	Root,
	ShortcutManager {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ColorChooserDialog.new.html">
	 *     gtk_color_chooser_dialog_new</a>
	 */
	constructor(window: Window?, title: String?) : this(
		gtk_color_chooser_dialog_new(
			title,
			window?.windowPointer
		)!!.reinterpret()
	)

	override val colorChooserPointer: CPointer<GtkColorChooser> by lazy {
		aboutDialogPointer.reinterpret()
	}

}