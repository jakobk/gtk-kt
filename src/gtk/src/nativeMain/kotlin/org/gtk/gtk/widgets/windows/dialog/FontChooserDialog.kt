package org.gtk.gtk.widgets.windows.dialog

import gtk.GtkFontChooser
import gtk.GtkFontChooserDialog_autoptr
import gtk.gtk_font_chooser_dialog_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gtk.*
import org.gtk.gtk.widgets.windows.Window

/**
 * kotlinx-gtk
 *
 * 08 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.FontChooserDialog.html">
 *     GtkFontChooserDialog</a>
 */
class FontChooserDialog(val fontChooserDialog: GtkFontChooserDialog_autoptr) :
	Dialog(fontChooserDialog.reinterpret()),
	Accessible,
	Buildable,
	ConstraintTarget,
	FontChooser,
	Native,
	Root,
	ShortcutManager {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.FontChooserDialog.new.html">
	 *     gtk_font_chooser_dialog_new</a>
	 */
	constructor(
		parent: Window? = null,
		title: String? = null
	) : this(
		gtk_font_chooser_dialog_new(
			title,
			parent?.windowPointer
		)!!.reinterpret()
	)

	override val fontChooserPointer: CPointer<GtkFontChooser> by lazy {
		fontChooserDialog.reinterpret()
	}
}